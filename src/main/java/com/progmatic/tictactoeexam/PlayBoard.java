package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

public class PlayBoard implements Board {

    public static final int ROW = 3;
    public static final int COL = 3;
    private PlayerType[][] aBoard = new PlayerType[COL][ROW];

    public PlayBoard() {
        for (int x = 0; x < COL; x++) {
            for (int y = 0; y < ROW; y++) {
                aBoard[x][y] = PlayerType.EMPTY;
            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        if (colIdx < 0 || colIdx >= aBoard.length || rowIdx < 0 || rowIdx >= aBoard[0].length) {
            throw new CellException(rowIdx, colIdx, "Nem jó! Adj meg jó kordinátákat.");
        }
        return aBoard[colIdx][rowIdx];
    }

    @Override
    public void put(Cell cell) throws CellException {
        if (cell.getCol() < 0 || cell.getCol() >= aBoard.length || cell.getRow() < 0 || cell.getRow() >= aBoard[0].length) {
            throw new CellException(cell.getRow(), cell.getCol(), "Nem jó! Adj meg jó kordinátákat.");
        }
        if (aBoard[cell.getCol()][cell.getRow()] != PlayerType.EMPTY) {
            throw new CellException(cell.getRow(), cell.getCol(), "Nem jó! Adj meg jó kordinátákat.");
        }
        aBoard[cell.getCol()][cell.getRow()] = cell.getCellsPlayer();
    }

    @Override
    public boolean hasWon(PlayerType p) {
        for (int i = 0; i < COL; i++) {
            if (aBoard[i][0] == aBoard[i][1]
                && aBoard[i][1] == aBoard[i][2]
                && aBoard[i][0] == p) {
                return true;
            }
        }
        for (int i = 0; i < ROW; i++) {
            if (aBoard[0][i] == aBoard[1][i] 
                  && aBoard[1][i] == aBoard[2][i] 
                  && aBoard[0][i] == p) {
                return true;
            }
        }
        if (aBoard[0][0] == aBoard[1][1] 
                && aBoard[1][1] == aBoard[2][2]
                && aBoard[1][1] == p) {
            return true;
        }
        if (aBoard[0][2] == aBoard[1][1]
                && aBoard[1][1] == aBoard[2][0]
                && aBoard[1][1] == p) {
            return true;
        }
        return false;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> empty = new ArrayList<>();
        for (int x = 0; x < COL; x++) {
            for (int y = 0; y < ROW; y++) {
                if (aBoard[x][y] == PlayerType.EMPTY) {
                    empty.add(new Cell(y, x, PlayerType.EMPTY));
                }
            }
        }
        return empty;
    }

}
