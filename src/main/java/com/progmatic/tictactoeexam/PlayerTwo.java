/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author progmatic
 */
public class PlayerTwo extends AbstractPlayer {

    public PlayerTwo(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {

        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                try {
                    if (b.getCell(y, x) == PlayerType.EMPTY) {
                        return new Cell(y, x, myType);
                    }
                } catch (CellException ex) {
                    Logger.getLogger(PlayerTwo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return null;
    }
}
