package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

public class PlayerOne extends AbstractPlayer {

    public PlayerOne(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {

        if (didHeWin(b) == true) {

            return null;
        }

        try {
            for (int x = 0; x < 3; x++) {
                for (int y = 0; y < 3; y++) {
                    if (b.getCell(y, x) == PlayerType.EMPTY) {
                        Board board2 = new PlayBoard();
                        for (int i = 0; i < 3; i++) {
                            for (int j = 0; j < 3; j++) {
                                if (b.getCell(i, j) != PlayerType.EMPTY) {
                                    board2.put(new Cell(i, j, b.getCell(i, j)));
                                }
                            }
                        }
                        board2.put(new Cell(y, x, myType));
                        if (board2.hasWon(myType)) {
                            return new Cell(y, x, myType);
                        }
                    }
                }
            }
        } catch (CellException e) {
            System.out.println("Something wrong!");
            return null;
        }

        try {
            for (int x = 0; x < 3; x++) {
                for (int y = 0; y < 3; y++) {
                    if (b.getCell(y, x) == PlayerType.EMPTY) {
                        return new Cell(y, x, myType);
                    }
                }
            }
        } catch (CellException e) {
            System.out.println("Something wrong!");
            return null;
        }

        return null;
    }

    private boolean didHeWin(Board b) {
        boolean win = false;
        if (b.hasWon(myType)) {
            win = true;
        }
        return win;
    }
}
